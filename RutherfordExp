import java.awt.*;
import java.awt.event.*;

public
class Rutherford extends Canvas implements Runnable 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2627346853714271370L;
	
	static final double epsilon = 8.85 * Math.pow(10, -12);	// przenikalnosc elektryczna prozni
	static final double eCharge = -1.601 * Math.pow(10, -19);	// ladunek elektronu [C]
	static final double alphaMass = 6.644656753 * Math.pow(10, -27); // masa czastki alfa [kg]
	static final double eMass = 9.11 * Math.pow(10,-31);		// masa elektronu [kg]
	static final double lightSpeed = 299792458;
	
	static final double convertMeVtoJ = 1.602*Math.pow(10,-13);// konwersja jednostek z MeV na J
	static final double convertmtofm = Math.pow(10,15);		// konwersja jednostek z m na fm
	
	static final double kqqConstant = eCharge*eCharge/(4*Math.PI*epsilon);	// stala do sily Coulomba
	static final double vConstant = convertMeVtoJ/Math.pow(lightSpeed, 2);	// stala do obliczen predkosci
	double accConstant;														// stala do obliczen przyspieszenia
	
	static final int canvasWidth = 400;  // szerokosc okna
	static final int canvasHeigth = 400; // wysokosc okna
	
	static final int R = 500;			// maksymalny parametr zderzenia [fm]
	static final int energyMax = 80;	// maksymalna energia kinetyczna [MeV]
	
	Button startButton, timeStepButton;
	
	int Z,z,b;			// ladunki jadra i czastki, parametr zderzenia
	double T,m;			// energia kinetyczna i masa
	double radius;		// rozmiar jadra [fm]s
	double d;			// odleglosc czastki od jadra [fm]
	double closest;		// najmniejsza odleglosc miedzy jadrem  [fm]
	int nucleusSize;	// rozmiar jadra w oknie do rysowania (nie sa zachowane proporcje - jadra powinno byc wieksze) [fm]

	boolean running = false;			// zmienna pozwalajaca zatrzymac/uruchomic symulacje
	boolean timeStep = false;

	double pixelsPerfm = canvasWidth/(2.0*R);	// liczba pikseli przypadajaca na 1 fm
	double xBeam,yBeam;					// polozenie czastki w oknie do rysowania [fm]
	double x,y,vx,vy,ax,ay;				// skladowe polozenia, predkosci, przyspiesznia
	double timeV = 10;				// stala uzyta do zmiennego kroku czasowego
	double dt;							// krok czasowy
	double v;							// predkosc czastki

	
	Rutherford() // Konstruktor domyslny
	{
		setSize(canvasWidth, canvasWidth);
		
		Frame pictureFrame = new Frame("Eksperyment Rutherforda");	// tworzenie okna z aplikacja
		pictureFrame.addWindowListener(new WindowAdapter() 
		{
			public void windowClosing(WindowEvent e)
			{
				System.exit(0);
		
			}
		});
		
		// Tymczasowe gui, potem podepniemy symulacje do bardziej rozbudowanego
		
		// Tworzenie okna do rysowania
		Panel canvasPanel = new Panel();				
		canvasPanel.add(this);							
		pictureFrame.add(canvasPanel);
		canvasPanel.setBackground(Color.GRAY);
		
		// Reszta - przycisk start,
		Panel scrollbarPanel = new Panel();
		Panel leftPanel = new Panel();
		scrollbarPanel.setLayout(new GridLayout(0,1));

		// Tworzenie przycisku start
		startButton = new Button("Start");					
		startButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				running = !running;
				if (running) startButton.setLabel("Stop");
				else startButton.setLabel("Start");
			}
		});

		leftPanel.add(startButton);
		
		timeStepButton = new Button("Time step - constant");					
		timeStepButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				if (timeStep)
				{
					timeStep = !timeStep;
					timeStepButton.setLabel("Time step - variable");
				}
				else
				{
					timeStep = !timeStep;
					timeStepButton.setLabel("Time step - constant");
				}
			}
		});

		leftPanel.add(timeStepButton);
							
		initialize();

		pictureFrame.add(scrollbarPanel, BorderLayout.SOUTH);
		pictureFrame.add(leftPanel, BorderLayout.WEST);
		pictureFrame.setResizable(false);
		pictureFrame.pack();
		pictureFrame.setVisible(true);
		
		Thread myThread = new Thread(this);
		myThread.start();
	} // Koniec konstruktora Rutherford()
	
	public void paint (Graphics g) 
	{
		// Rysowanie jadra zlota
		g.setColor(Color.yellow);
		nucleusSize = 83*10/83;
		g.fillOval(canvasWidth/2 - nucleusSize, canvasWidth/2 - nucleusSize, 2*nucleusSize,2*nucleusSize);
		
		// Rysowanie "strumienia" czastek
		g.setColor(Color.red);							
		g.fillOval((int) Math.round((xBeam+500) * pixelsPerfm - 5), (int) Math.round((yBeam+500)*pixelsPerfm - 5), 10, 10);
			
		// Rysowanie emitera
		g.setColor(Color.white);							
		g.fillRect(0, (int) Math.round((b + 500) * pixelsPerfm - 10), 10, 20);
			
		// Wyswietlanie wymiaru okna
		g.setColor(Color.orange);
		g.drawString("1000 fm X 1000 fm", 280, canvasWidth);


	} // Koniec metody paint
	
	void initialize() 
	{
		T = 2.5;
		Z = 79;
		z = 2;
		b = -100;
		m = alphaMass;
		
		/* r = 1.18A^1/3 - .48 fm (strona 46 "Nuclear & Particle Physics" Williams)
		 A w przyblizeniu 2Z */
		
		radius = 1.18 * Math.pow(2 * Z, 1.0 / 3) - 0.48;

		xBeam = -460;			// fm
		yBeam = b;
		closest = Math.sqrt(xBeam*xBeam + yBeam*yBeam);
		
		x = xBeam / convertmtofm;		
		y = yBeam / convertmtofm;
		
		// Relatywistyczny wzor na energie kinetyczna: T = (gamma - 1)mc^2
		vx = lightSpeed * Math.sqrt(1 - Math.pow(T * vConstant / m + 1, -2));
		vy = 0;
		
		computeAccelerations();
	} // Koniec metody initialize()
	
	void computeAccelerations() 
	{
			d = Math.sqrt(xBeam*xBeam + yBeam*yBeam);
			if (d < closest) closest = d;
			
			if (d < radius) 
			{		
				accConstant = kqqConstant * Z * z / (Math.pow (radius / convertmtofm,3) * m);
				ax = accConstant * x;
				ay = accConstant * y;
			}
			else 
			{
				accConstant = kqqConstant * Z * z / (Math.pow((x * x + y * y) , 1.5) * m);
				ax = accConstant * x;
				ay = accConstant * y;
			}	 
	} // Koniec metody computeAccelerations()
	
	void doStep() 
	{
		if (timeStep) dt = Math.pow(10, -25);
		else dt = timeV / Math.sqrt(ax * ax + ay * ay);
				
		x += vx * dt + 0.5 * ax * dt * dt;	
		y += vy * dt + 0.5 * ay * dt * dt;
		
		vx += 0.5 * ax * dt;			
		vy += 0.5 * ay * dt;
		
		computeAccelerations();		
		
		vx += 0.5 * ax * dt;			
		vy += 0.5 * ay * dt;
		
		xBeam = x * convertmtofm;
		yBeam = y * convertmtofm;
	} // Koniec metody doStep()
	
	public
	void run() 
	{
		while (true) 
		{
			initialize();
			repaint();
			
			while(Math.abs(xBeam) < R && Math.abs(yBeam) < R)
			{	
				if (running) for (int k=0; k<5000; k++) doStep();
				repaint();
				try {Thread.sleep(1);} catch (InterruptedException e) {}
			}
		}
	} // Koniec metody run()
	
	public 
	static void main(String[] arg) 
	{
		new Rutherford();
	} // Koniec maina
}
