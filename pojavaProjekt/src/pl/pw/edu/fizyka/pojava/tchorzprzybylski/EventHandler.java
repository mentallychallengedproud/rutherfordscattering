package pl.pw.edu.fizyka.pojava.tchorzprzybylski;



import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;

public class EventHandler {

	boolean running = false;	
	@FXML
	private TextArea outputTextArea;
	@FXML
	private Button startButton;
	@FXML
	private Button startButton1;
	@FXML
	private Button startButton2;
	@FXML
	private Button startButton3;
	@FXML
	private void handleStartButtonAction(ActionEvent e) {
		
		  
		running = !running;
	
		if (running) startButton.setText("Stop");
		else startButton.setText("Start");
}
	@FXML
	private void handleStartButton1Action(ActionEvent e) {
		
		  
		running = !running;
	
		if (running) startButton1.setText("Stop");
		else startButton1.setText("Start");
}
	@FXML
	private void handleStartButton2Action(ActionEvent e) {
		
		  
		running = !running;
	
		if (running) startButton2.setText("Stop");
		else startButton2.setText("Start");
}
	@FXML
	private void handleStartButton3Action(ActionEvent e) {
		
		  
		running = !running;
	
		if (running) startButton3.setText("Stop");
		else startButton3.setText("Start");
}
}
